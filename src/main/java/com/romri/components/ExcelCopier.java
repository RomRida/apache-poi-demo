package com.romri.components;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * Interface implementing copying functionality for Apache POI excel interpretation.
 */
public interface ExcelCopier {
    /**
     * Method for copying standard values from one cell to another.
     *
     * @param sourceCell      cell that you want to copy
     * @param destinationCell cell that you want to put copied value
     * @throws IllegalArgumentException if value in <i>sourceCell</i> have unknown origin
     */
    default void copyCell(Cell sourceCell, Cell destinationCell) {
        switch (sourceCell.getCellType()) {
            case NUMERIC -> destinationCell.setCellValue(sourceCell.getNumericCellValue());
            case STRING -> destinationCell.setCellValue(sourceCell.getStringCellValue());
            case BOOLEAN -> destinationCell.setCellValue(sourceCell.getBooleanCellValue());
            case ERROR -> destinationCell.setCellValue(sourceCell.getErrorCellValue());
            case FORMULA -> destinationCell.setCellValue(sourceCell.getCellFormula());
            case BLANK -> destinationCell.setBlank();
            default -> throw new IllegalArgumentException("Unexpected value: " + sourceCell.getCellType());
        }
    }

    /**
     * Method for copying all values and styles from all modified rows.
     * If cell was modified and left without value, it will still copy it.
     *
     * @param sourceWorkbook      workbook that you want to copy rows from
     * @param destinationWorkbook workbook that you want to put copied values
     */
    default void copyAllRows(Workbook sourceWorkbook, Workbook destinationWorkbook) {
        Sheet sourceSheet = sourceWorkbook.getSheetAt(0);
        Sheet destinationSheet = destinationWorkbook.getSheetAt(0);
        for (Row sourceRow : sourceSheet) {
            Row destinationRow = destinationSheet.createRow(destinationSheet.getLastRowNum() + 1);
            for (Cell sourceCell : sourceRow) {
                Cell destinationCell = destinationRow.createCell(sourceCell.getColumnIndex());
                copyCellStyle(destinationWorkbook, sourceCell, destinationCell);
                copyCell(sourceCell, destinationCell);
            }
        }
    }

    /**
     * Method for copying cell style.
     * If cell was modified and left without value, it will still copy style from it.
     *
     * @param destinationWorkbook workbook where you want to copy the style
     * @param sourceCell          cell that you want to copy style from
     * @param destinationCell     cell that you want to apply copied style
     */
    default void copyCellStyle(Workbook destinationWorkbook, Cell sourceCell, Cell destinationCell) {
        CellStyle destinationCellStyle = destinationWorkbook.createCellStyle();
        destinationCellStyle.cloneStyleFrom(sourceCell.getCellStyle());
        destinationCell.setCellStyle(destinationCellStyle);
    }


    /**
     * Method for copying all merged cells from one workbook to another.
     *
     * @param sourceWorkbook      workbook with merged cells to copy
     * @param destinationWorkbook workbook in which it is planned to transfer copied merged cells
     */
    default void copyAllMergedCells(Workbook sourceWorkbook, Workbook destinationWorkbook) {
        Sheet sourceSheet = sourceWorkbook.getSheetAt(0);
        Sheet destinationSheet = destinationWorkbook.getSheetAt(0);
        for (CellRangeAddress sourceCellRangeAddress : sourceSheet.getMergedRegions()) {
            destinationSheet.addMergedRegion(sourceCellRangeAddress);
        }
    }

    /**
     * Method for copying all cells height and width from one work book to another.
     *
     * @param sourceWorkbook      workbook to copy from all cells height and width
     * @param destinationWorkBook workbook in which it is planned to transfer the copied height and width of all cells
     */
    default void copyAllCellsHeightAndWidth(Workbook sourceWorkbook, Workbook destinationWorkBook) {
        Sheet sourceSheet = sourceWorkbook.getSheetAt(0);
        Sheet destinationSheet = destinationWorkBook.getSheetAt(0);
        for (Row sourceRow : sourceSheet) {
            Row destinationRow = destinationSheet.getRow(sourceRow.getRowNum());
            short sourceRowHeight = sourceRow.getHeight();
            destinationRow.setHeight(sourceRowHeight);
        }
        for (Cell sourceCell : sourceSheet.getRow(0)) {
            int sourceColumnWidth = sourceSheet.getColumnWidth(sourceCell.getColumnIndex());
            destinationSheet.setColumnWidth(sourceCell.getColumnIndex(), sourceColumnWidth);
        }
    }

}
