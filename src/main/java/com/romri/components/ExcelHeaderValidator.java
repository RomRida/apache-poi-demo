package com.romri.components;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Class for validation of Excel header compared to specified reference for Apache POI excel interpretation.
 */
@Component
public class ExcelHeaderValidator implements ExcelComparator {

    private final Workbook headerExampleWorkbook;

    /**
     * Constructor that will instantiate workbook with given path to valid Excel file containing header.
     *
     * @param pathToValidFileWithHeader path to Excel file with header that other Excel file would compare to
     */
    public ExcelHeaderValidator(@Value("${location.header}") String pathToValidFileWithHeader) {
        try (Workbook sourceWorkbook = WorkbookFactory.create(new File(pathToValidFileWithHeader))){
            this.headerExampleWorkbook = sourceWorkbook;
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Method that validates header of Excel file.
     *
     * @param file Excel file in which header need to be validated
     * @return True if header is valid, false otherwise
     */
    public boolean isHeaderValid(File file) {
        try (Workbook workbookToCheck = WorkbookFactory.create(file)) {
            Sheet sourceSheet = headerExampleWorkbook.getSheetAt(0);
            Sheet sheetToCheck = workbookToCheck.getSheetAt(0);
            return compareSheets(sourceSheet, sheetToCheck);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
