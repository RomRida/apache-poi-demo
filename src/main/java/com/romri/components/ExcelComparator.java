package com.romri.components;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * Interface implementing comparison functionality for Apache POI excel interpretation.
 */
public interface ExcelComparator {

    /**
     * Method that compares two cells by type, address, style and value.
     *
     * @param sourceCell    is a reference cell with which you want to compare
     * @param cellToCompere cell that you will compare with the reference cell
     * @return true if the cells are the same for all compared indicators and false otherwise
     */
    default boolean compareCells(Cell sourceCell, Cell cellToCompere) {
        return sourceCell.getCellType().equals(cellToCompere.getCellType()) &&
                sourceCell.getAddress().equals(cellToCompere.getAddress()) &&
                sourceCell.getCellStyle().equals(cellToCompere.getCellStyle()) &&
                sourceCell.getStringCellValue().equals(cellToCompere.getStringCellValue());
    }

    /**
     * Method that compares two rows by each cell.
     *
     * @param sourceRow    is a reference row with which you want to compare
     * @param rowToCompere row that you will compare with the reference row
     * @return true if all cells in the rows are the same and false otherwise
     */
    default boolean compareRows(Row sourceRow, Row rowToCompere) {
        boolean rowsAreSame = true;
        if (sourceRow.getLastCellNum() == rowToCompere.getLastCellNum()) {
            for (Cell sourceCell : sourceRow) {
                Cell cellToCheck = rowToCompere.getCell(sourceCell.getColumnIndex());
                if (!(compareCells(sourceCell, cellToCheck))) {
                    rowsAreSame = false;
                }
            }
        } else {
            rowsAreSame = false;
        }
        return rowsAreSame;
    }

    /**
     * Method that compares two sheets by each row.
     *
     * @param sourceSheet    is a reference sheet with which you want to compare
     * @param sheetToCompere sheet that you will compare with the reference sheet
     * @return true if all rows in the sheets are the same and false otherwise
     */
    default boolean compareSheets(Sheet sourceSheet, Sheet sheetToCompere) {
        boolean sheetsAreSame = true;
        if (sheetToCompere.getLastRowNum() >= sourceSheet.getLastRowNum()) {
            for (int rowNumber = 0; rowNumber < sourceSheet.getLastRowNum(); rowNumber++) {
                if (!compareRows(sourceSheet.getRow(rowNumber), sheetToCompere.getRow(rowNumber))) {
                    sheetsAreSame = false;
                    break;
                }
            }
        } else {
            sheetsAreSame = false;
        }
        return sheetsAreSame;
    }
}
