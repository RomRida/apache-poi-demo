package com.romri.components;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface implementing functionality for detecting and deleting empty rows in Apache POI excel interpretation.
 */
public interface EmptyTrimmer {

    /**
     * Method that deletes all empty rows in a sheet.
     *
     * @param sheet is a sheet in which you want to remove all empty rows from
     */
    default void trimEmptyRows(Sheet sheet) {
        List<Row> listOfEmptyRows = new ArrayList<>();
        for (Row row : sheet) {
            if (isRowEmpty(row)) {
                listOfEmptyRows.add(row);
            }
        }
        for (Row row : listOfEmptyRows) {
            sheet.removeRow(row);
        }
    }

    /**
     * Method to determine if a row is empty.
     *
     * @param row is a row that you want to check
     * @return True if row empty and false otherwise
     */
    default boolean isRowEmpty(Row row) {
        boolean isEmpty = true;
        DataFormatter dataFormatter = new DataFormatter();
        if (row != null) {
            for (Cell cell : row) {
                if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
                    isEmpty = false;
                    break;
                }
            }
        }
        return isEmpty;
    }
}
