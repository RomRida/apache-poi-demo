package com.romri.components;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Class to accumulate all workbooks that it receives through
 * <i>accumulateWorkbooks</i> method into single workbook with
 * consistent header for Apache POI excel interpretation.
 */
@Component
public class Accumulator implements ExcelCopier {
    private final String header;
    private final Workbook accumulatedWorkbook;

    /**
     * Constructor that will instantiate workbook instance and
     * path to correct header.
     *
     * @param pathToValidFileWithHeader path to header that will be in accumulated Excel file
     */
    public Accumulator(@Value("${location.header}") String pathToValidFileWithHeader) {
        this.header = pathToValidFileWithHeader;
        this.accumulatedWorkbook = new XSSFWorkbook();
    }

    /**
     * Method to accumulate workbooks into single file, every
     * workbook that passed adds up to <i>accumulatedWorkbook</i>.
     *
     * @param workbookToAccumulate workbook that you want to add to accumulation
     */
    public void accumulateWorkbooks(Workbook workbookToAccumulate) {
        if (accumulatedWorkbook.getNumberOfSheets() == 0) {
            accumulatedWorkbook.createSheet();
            copyHeader(new File(header));
        } else {
            copyAllRows(workbookToAccumulate, accumulatedWorkbook);
        }
    }

    /**
     * Method that returns instance of accumulated workbook.
     *
     * @return Instance of accumulated workbook
     */
    public Workbook getAccumulatedWorkbook() {
        return accumulatedWorkbook;
    }

    /**
     * Method for copying header from file with correct header.
     *
     * @param sourceFile file with header that you want to copy
     */
    private void copyHeader(File sourceFile) {
        try (Workbook workbookWithHeader = WorkbookFactory.create(sourceFile)) {
            copyAllMergedCells(workbookWithHeader, accumulatedWorkbook);
            copyAllRows(workbookWithHeader, accumulatedWorkbook);
            copyAllCellsHeightAndWidth(workbookWithHeader, accumulatedWorkbook);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
