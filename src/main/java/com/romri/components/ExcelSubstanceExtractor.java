package com.romri.components;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Class that extracts only rows containing information and skips headers from Excel file for Apache POI excel interpretation.
 */
@Component
public class ExcelSubstanceExtractor implements ExcelCopier, EmptyTrimmer {
    private final int numberOfRowsToSkip;

    /**
     * Constructor that calculates how many rows needed to be skipped inside each file with valid header
     *
     * @param pathToValidFileWithHeader path to valid header
     */
    public ExcelSubstanceExtractor(@Value("${location.header}") String pathToValidFileWithHeader) {
        try (Workbook sourceWorkbook = WorkbookFactory.create(new File(pathToValidFileWithHeader))) {
            this.numberOfRowsToSkip = sourceWorkbook.getSheetAt(0).getPhysicalNumberOfRows();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Method that skips header and extracts only rows with information inside Excel file
     *
     * @param sourceFile path to file that you want to extract information from
     * @return Workbook with only rows containing information
     */
    public Workbook extractOnlySubstanceFromFile(File sourceFile) {
        Workbook destinationWorkbook = new XSSFWorkbook();
        Sheet destinationSheet = destinationWorkbook.createSheet();
        try (Workbook sourceWorkbook = WorkbookFactory.create(sourceFile)) {
            Sheet sourceSheet = sourceWorkbook.getSheetAt(0);
            trimEmptyRows(sourceSheet);
            for (int rowNumber = numberOfRowsToSkip; rowNumber < sourceSheet.getPhysicalNumberOfRows(); rowNumber++) {
                Row destinationRow = destinationSheet.createRow(rowNumber - numberOfRowsToSkip);
                for (Cell sourceCell : sourceSheet.getRow(rowNumber)) {
                    Cell destinationCell = destinationRow.createCell(sourceCell.getColumnIndex());
                    copyCellStyle(destinationWorkbook, sourceCell, destinationCell);
                    copyCell(sourceCell, destinationCell);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destinationWorkbook;
    }
}
