package com.romri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ApachePoiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApachePoiDemoApplication.class, args);
    }

}

