package com.romri.services;

import com.romri.components.Accumulator;
import com.romri.components.ExcelHeaderValidator;
import com.romri.components.ExcelSubstanceExtractor;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.apache.commons.math3.util.MathUtils.checkNotNull;

/**
 * Service for Excel files aggregation.
 * <p>
 * This class handles aggregation of several Excel files into one
 * as long as they meet all given conditions. Works with Apache POI excel interpretation.
 * </p>
 */
@Log4j2
@Service
public class ExcelFileAggregatorService {

    private final ExcelSubstanceExtractor extractor;
    private final Accumulator accumulator;
    private final ExcelHeaderValidator validator;
    private final File sourceDirectory;

    /**
     * Constructor that instantiates all needed objects and directory from with files will be accumulated.
     *
     * @param extractor       extracts substance from each file
     * @param accumulator     accumulates each file into one result file
     * @param validator       validates each file for tha correct header
     * @param sourceDirectory - directory in which files, that need accumulation are located
     */
    public ExcelFileAggregatorService(ExcelSubstanceExtractor extractor,
                                      Accumulator accumulator,
                                      ExcelHeaderValidator validator,
                                      @Value("${location.excel}") File sourceDirectory) {
        this.extractor = extractor;
        this.accumulator = accumulator;
        this.validator = validator;
        this.sourceDirectory = sourceDirectory;
    }

    /**
     * Method that extracts file extension.
     *
     * @param fullName files full name with extension
     * @return Extension of file
     */
    private static String getFileExtension(String fullName) {
        checkNotNull(fullName);
        String fileName = new File(fullName).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    /**
     * Method that checks if a directory is empty.
     *
     * @param folder directory that needs to be checked
     * @return True if directory is empty, false otherwise
     */
    private static boolean isFolderEmpty(String[] folder) {
        if (folder == null) {
            return true;
        } else {
            return folder.length == 0;
        }
    }

    /**
     * Method that tries to create aggregated file over specified periods of time.
     *
     * @throws FileNotFoundException    if specified source directory with Excel files does not exist
     * @throws IllegalArgumentException if specified source directory is empty
     */
    @Scheduled(cron = "${cron.expression}")
    public void createAggregatedDataFile() throws FileNotFoundException {
        log.info("Specified file path to directory is {}", sourceDirectory.getAbsolutePath());
        if (sourceDirectory.exists() && sourceDirectory.isDirectory()) {
            log.info("Working in directory {}", sourceDirectory.getAbsolutePath());
            String[] filesInsideDirectory = sourceDirectory.list();
            if (!isFolderEmpty(filesInsideDirectory)) {
                iterateOverFolder(sourceDirectory);
            } else {
                throw new IllegalArgumentException(sourceDirectory.getAbsolutePath() + " directory is empty.");
            }
            writeToFile(sourceDirectory, accumulator.getAccumulatedWorkbook());
        } else {
            throw new FileNotFoundException(sourceDirectory.getAbsolutePath() + " directory specified does not exist.");
        }
    }

    /**
     * Method that iterates over file inside folder.
     * If file is valid Excel file it will try to aggregate information from it.
     *
     * @param sourceDirectory directory in with all files that needs to be iterated over are located
     */
    private void iterateOverFolder(File sourceDirectory) {
        String[] filesInsideDirectory = sourceDirectory.list();
        if (!isFolderEmpty(filesInsideDirectory)) {
            for (String filename : filesInsideDirectory) {
                String fileFormat = getFileExtension(filename);
                if (fileFormat.equals("xlsx") || fileFormat.equals("xls")) {
                    log.info("Reading file {}", filename);
                    File sourceFile = new File(sourceDirectory.getAbsolutePath(), filename);
                    aggregateValidFiles(sourceFile);
                } else {
                    log.error("{} is not supported file format", filename);
                }
            }
        } else {
            throw new IllegalArgumentException(sourceDirectory.getAbsolutePath() + " directory is empty.");
        }
    }

    /**
     * Method that aggregates workbook with valid header in one accumulation workbook.
     * If header is invalid it does not throw exception, only logs name of a file with invalid header.
     *
     * @param sourceFile Excel file that need to be accumulated
     */
    private void aggregateValidFiles(File sourceFile) {
        if (validator.isHeaderValid(sourceFile)) {
            Workbook substanceFromExcelFile = extractor.extractOnlySubstanceFromFile(sourceFile);
            accumulator.accumulateWorkbooks(substanceFromExcelFile);
        } else {
            log.error("{} header isn't valid", sourceFile.getName());
        }
    }

    /**
     * Method that writes accumulated workbook as an Excel file in a separate directory under the source directory.
     *
     * @param sourceDirectory directory in with new directory with result will be created
     * @param workbook        workbook that will be written in directory
     */
    private void writeToFile(File sourceDirectory, Workbook workbook) {
        File resultDirectory = new File(sourceDirectory.getAbsolutePath(), "result");
        if (!resultDirectory.exists()) {
            resultDirectory.mkdir();
        }

        try (FileOutputStream out = new FileOutputStream(resultDirectory.getAbsolutePath() + "\\result.xlsx")) {
            workbook.write(out);
            log.info("result.xlsx written successfully on disk.");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
