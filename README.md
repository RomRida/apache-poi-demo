# Apache POI demo

## Description
Demo Apache POI project for skill test.
It will look in the specified directory and at a certain interval (directory and time interval specified in application.properties) collect all valid EXCEL files into a single file.

## Usage
For now, to use this project open it in you IDE of choice and launch as ordinary Spring Boot application  

## Project status
In development
